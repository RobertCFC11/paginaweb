import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoguiadoComponent } from './noguiado.component';

describe('NoguiadoComponent', () => {
  let component: NoguiadoComponent;
  let fixture: ComponentFixture<NoguiadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoguiadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoguiadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
