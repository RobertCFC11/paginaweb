import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuiadosComponent } from './guiados.component';

describe('GuiadosComponent', () => {
  let component: GuiadosComponent;
  let fixture: ComponentFixture<GuiadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuiadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuiadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
