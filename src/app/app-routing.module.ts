import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';
import { MediosComponent } from './medios/medios.component';
import { GuiadosComponent } from './guiados/guiados.component';
import { NoguiadoComponent } from './noguiado/noguiado.component';
import { ReferenciaComponent } from './referencia/referencia.component';


const routes: Routes = [
    { path: 'principal', component: PrincipalComponent },
    { path: 'medios', component: MediosComponent },
    { path: 'guiados', component: GuiadosComponent },
    { path: 'no guiados', component: NoguiadoComponent },
    { path: 'bibliografia', component: ReferenciaComponent },
    
  
    { path: '**', pathMatch: 'full', redirectTo: 'principal'},
    // { path: '**', component: PageNotFoundComponent },
  ];
  
  @NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  