import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PrincipalComponent } from './principal/principal.component';
import { MediosComponent } from './medios/medios.component';
import { GuiadosComponent } from './guiados/guiados.component';
import { ReferenciaComponent } from './referencia/referencia.component';
import { NoguiadoComponent } from './noguiado/noguiado.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PrincipalComponent,
    MediosComponent,
    GuiadosComponent,
    ReferenciaComponent,
    NoguiadoComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
